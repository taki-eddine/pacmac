#!/usr/bin/bash

[[ -n "${LIBPACMAC_TIDY_SH}" ]] && return
LIBPACMAC_TIDY_SH=1

#***************************************************************************************************
source "/usr/share/makepkg/util/message.sh"
#***************************************************************************************************


packaging_options=()
tidy_remove=()
tidy_modify=()

for lib in "${LIBPACMAC}/tidy/"*.sh; do
	source "${lib}"
done

readonly -a packaging_options tidy_remove tidy_modify


############################################################
function tidy_install() {
	cd_safe "${pkgdir}"
	msg "$(gettext "Tidying install...")"

	# options that remove unwanted files
	for func in ${tidy_remove[@]}; do
		${func}
	done

	# options that modify files
	for func in ${tidy_modify[@]}; do
		${func}
	done
}
