#!/bin/bash

[[ -n "${LIBPACMAC_UTIL_UTIL_SH}" ]] && return
LIBPACMAC_UTIL_UTIL_SH=1

#***************************************************************************************************
source "/usr/share/makepkg/util/message.sh"

source "/usr/share/brush/util/log.sh"
#***************************************************************************************************

############################################################
### \brief Read configurations from file.
### \param pacmac_config The configuration file.
############################################################
function read_configuration() {
    local pacmac_config="${1}"

    if [[ -f "${pacmac_config}" ]]; then
        source "${pacmac_config}"
    else
        error "No configurations file was set."
        exit 1
    fi

    CUSTOM_REPO_ANY="${CUSTOM_REPO}/any"
    CUSTOM_REPO_X86_64="${CUSTOM_REPO}/x86_64"
}

############################################################
function source_safe() {
    :;
}


############################################################
### \brief Convert string depending on pattern.
############################################################
function convert_pattern() {
    local orignal_pattern="${1}"
    local target_pattern="${2}"
    local versison="${3}"
    sed -r "s/${orignal_pattern}/${target_pattern}/" <<< "${version}"
}


############################################################
### \brief Fetchs version online follwing a regex patern.
############################################################
function fetch_version() {
    local url
    local search_pattern="((?<=tags/|tags/v)[0-9.]+$)"
    local convert_pattern
    local sort_criterias=()
    local version

    local url_data
    local versions=()

    ##
    while (( ${#} )); do
        case "${1}" in
            --url=*)
                url="${1#--url=}"
                ;;
            --regex=*)
                search_pattern="${1#--regex=}"
                ;;
            --convert=*)
                convert_pattern="${1#--convert=}"
                ;;
            --sort=*)
                sort_criterias=(${1#--sort=})
                ;;
        esac
        shift
    done

    # get site data.
    brush::log.d "fetch_version()" "url = '${url}'"
    if [[ -n "${url}" ]]; then
        local cmd

        case ${url} in
            *.git )
                cmd="git ls-remote --tags"
                ;;

            * )
                cmd="wget -qO-"
        esac

        brush::log.i "fetch_version()" "${cmd} '${url}'"
        url_data=$(${cmd} "${url}")
    else
        error "'--url' option is mandatory."
        exit 1
    fi

    # grep versions from site.
    brush::log.d "fetch_version()" "search_pattern = ${search_pattern}"
    if [[ -n "${search_pattern}" ]]; then
        versions=($(grep -oP "${search_pattern}" <<< "${url_data}" | uniq | tr '\n' ' '))
    else
        error "'--regex' option is mandatory."
        exit 1
    fi
    brush::log.d "fetch_version()" "versions = ${versions[@]}"

    # convert the version to appropriete style.
    brush::log.d "fetch_version()" "convert_pattern = ${convert_pattern}"
    if [[ -n "${convert_pattern}" ]]; then
        versions=($(tr ' ' '\n' <<< "${versions[@]}" | sed -r ${convert_pattern} | tr '\n' ' ' ))
    fi
    brush::log.d "fetch_version()" "versions = ${versions[@]}"

    # sort the versions.
    brush::log.d "fetch_version()" "sort_criterias = (${sort_criterias[@]})"
    versions=($(tr ' ' '\n' <<< "${versions[@]}" | sort -t'.' --version-sort ${sort_criterias[@]} | tr '\n' ' ' ))
    brush::log.d "fetch_version()" "versions = ${versions[@]}"

    # get the latest version.
    version=$(tr ' ' '\n' <<< "${versions[@]}" | tail -1)
    brush::log.d "fetch_version()" "version = ${version}"

    echo "${version}"
}


############################################################
function find_any_tarball() {
    local startdir="${1}"

    ls "${startdir}/"*.pkg.tar.xz 2> /dev/null
}


############################################################
### \breif Gives pkgname from absoulte path.
############################################################
function pkgname() {
    local pkgpath="${1}"
    sed -r 's:.*/::; s:-r?[0-9].*::' <<< "${pkgpath}" 2> /dev/null
}


############################################################
function find_package_tarball() {
    local startdir="${1}"
    local pkgname="${2}"
    ls "${startdir}/${pkgname}"-[r0-9]*-*-*.pkg.tar.xz 2> /dev/null
}


############################################################
### \brief Create the linux directory base tree and extra directories.
### \param ${@} Pattern of extra directories to create it.
############################################################
function make_base_tree_dir() {
    install -d "${pkgdir}"/etc/profile.d
    install -d "${pkgdir}"/opt/
    install -d "${pkgdir}"/usr/{bin,lib,share}
    install -d "${pkgdir}"/usr/share/{applications,icons,pixmaps,themes}

    if [[ ${#} -ne 0 ]]; then
        install -d "${@}"
    fi
}
