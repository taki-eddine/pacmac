#!/bin/bash

[[ -n "${LIBPACMAC_SOURCE_SH}" ]] && return
LIBPACMAC_SOURCE_SH=1

#***************************************************************************************************
LIBRARY=${LIBRARY:-'/usr/share/makepkg'}

source "$LIBRARY/util/message.sh"
source "$LIBRARY/util/pkgbuild.sh"
source "$LIBRARY/util/source.sh"

for lib in "$LIBRARY/source/"*.sh; do
	source "$lib"
done

LIBPACMAC="${LIBPACMAC:=/usr/share/pacmac}"
for lib in "${LIBPACMAC}/source/"*.sh; do
    source "${lib}"
done
#***************************************************************************************************

############################################################
function extract_sources_files() {
	msg "$(gettext "Extracting sources...")"
	local netfile
    local all_sources

	get_all_sources_for_arch 'all_sources'
	for netfile in "${all_sources[@]}"; do
		local file=$(get_filename "${netfile}")
		local proto=$(get_protocol "${netfile}")

		case "${proto}" in
			bzr*)
				extract_bzr "${netfile}"
				;;
			git*)
				extract_git "${netfile}"
				;;
			hg*)
				extract_hg "${netfile}"
				;;
			svn*)
				extract_svn "${netfile}"
				;;
			*)
				extract_compressed "${file}"
				;;
		esac
	done
}
