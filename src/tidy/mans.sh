#!/usr/share/bash

[[ -n "${LIBPACMAC_TIDY_MANS_SH}" ]] && return
LIBPACMAC_TIDY_MANS_SH=1

#***************************************************************************************************
source "/usr/share/makepkg/util/message.sh"
source "/usr/share/makepkg/util/option.sh"
#***************************************************************************************************


packaging_options+=('mans')
tidy_remove+=('tidy_mans')


############################################################
function tidy_mans() {
    if check_option "mans" "n"; then
        msg2 "Removing mans files..."
        brush::log.d "mans.sh @ tidy_mans()" "PWD = ${PWD}"
        rm -rf "usr/share/man"
    fi
}
