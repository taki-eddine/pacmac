#!/bin/bash

[[ -n "${LIBPACMAC_UTIL_SH}" ]] && return
LIBPACMAC_UTIL_SH=1

#***************************************************************************************************
LIBPACMAC="${LIBPACMAC:=/usr/share/pacmac}"
for lib in "${LIBPACMAC}/util/"*.sh; do
    source "${lib}"
done
#***************************************************************************************************
